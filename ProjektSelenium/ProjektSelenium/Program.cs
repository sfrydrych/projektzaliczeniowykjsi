﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProjektSelenium
{
    class Program
    {
        static void Main(string[] args)
        {
            ///////////////////////////////////////////////////////////////////////////// POCZĄTEK ///////////////////////////////////////////////////////////////////////////////////



            ///* Google Chrome */
            //IWebDriver driver = new ChromeDriver();

            /* Mozilla Firefox */
            IWebDriver driver = new FirefoxDriver();

            // Załadowanie sterownika FirefoxDriver i przejście do strony www.wp.pl
            driver.Url = ("https://profil.wp.pl/login.html?zaloguj=poczta");
            
            // Maksymalizacja okna przeglądarki
            driver.Manage().Window.Maximize();

            // Wprowadzenie loginu i hasła użytkownika, a następnie wciśnięcie przycisku "Enter"
            driver.FindElement(By.Name("login_username")).SendKeys("poczta@wp.pl");
            Thread.Sleep(2000);
            driver.FindElement(By.Name("password")).SendKeys("haslo" + Keys.Enter);
            Thread.Sleep(5000);



            ////////////////////////////////////////////////////////// TEST 1 - WYŚWIETLANIE LISTY MAILI WYSŁANYCH ///////////////////////////////////////////////////////////////////



            // Przejście do zakładki "Wysłane"
            driver.FindElement(By.XPath("//*[@id='folder-2']/div[2]")).Click();
            Thread.Sleep(5000);

            // Zliczenie wszystkich maili na stronie i wyświetlenie ich danych w konsoli 
            IList<IWebElement> mails = driver.FindElements(By.XPath("//div[@class='stream-item__info']"));
            Console.WriteLine();
            Console.WriteLine("Ilość maili: " + mails.Count);
            Thread.Sleep(2000);
            Console.WriteLine();
            for (int i = 0; i < mails.Count; i++)
            {
                Console.WriteLine(mails[i].Text);
                Console.WriteLine();
            }
            Thread.Sleep(5000);



            ////////////////////////////////////////////////////////// TEST 2 - ODCZYTANIE TREŚCI KONKRETNEGO MAILA //////////////////////////////////////////////////////////////////



            // Przejście do zakładki "Wysłane"
            driver.FindElement(By.XPath("//*[@id='folder-2']/div[2]")).Click();
            Thread.Sleep(5000);

            // Przejście do ostatniego wysłanego maila i wyświetlenie jego treści w konsoli
            driver.FindElement(By.XPath("/html/body/div[2]/nh-app-view/div/div/div/div[1]/div/div/div/div/div/div[2]/div[1]/div[1]/div[1]/nh-stream-item[1]/div/div/div/div/div[7]/div/div")).Click();
            Thread.Sleep(2000);
            var mailContent = driver.FindElement(By.XPath("//*[@id='gwp890a5c89h']/div/div"));
            Console.WriteLine(mailContent.Text);
            Thread.Sleep(5000);



            ////////////////////////////////////////////////////////// TEST 3 - CAŁKOWITE USUWANIE KONKRETNEGO MAILA /////////////////////////////////////////////////////////////////



            // Przejście do zakładki "Odebrane"
            driver.FindElement(By.XPath("/html/body/div[2]/nh-app-view/div/div/div/nh-sidebar/div/div/div[3]/div[1]/div[1]/div[2]")).Click();
            Thread.Sleep(2000);

            // Zaznaczenie checkbox'a przy ostatnio odebranym mailu
            driver.FindElement(By.XPath("/html/body/div[2]/nh-app-view/div/div/div/div[1]/div/div/div/div/div[2]/div[2]/div[1]/div/div[1]/nh-stream-item[1]/div/div/div/div/div[1]/nh-checkbox/div")).Click();
            Thread.Sleep(2000);

            // Naciśnięcie przycisku "Usuń"
            driver.FindElement(By.XPath("/html/body/div[2]/nh-app-view/div/div/div/div[1]/div/div/div/div/nh-top-action-bar/div/div[1]/button[3]")).Click();
            Thread.Sleep(2000);

            // Przejście do zakładki "Kosz"
            driver.FindElement(By.XPath("/html/body/div[2]/nh-app-view/div/div/div/nh-sidebar/div/div/div[3]/div[1]/div[2]/div[2]")).Click();
            Thread.Sleep(2000);

            // Naciśnięcie przycisku "Opróżnij"
            driver.FindElement(By.XPath("/html/body/div[2]/nh-app-view/div/div/div/nh-sidebar/div/div/div[3]/div[1]/div[2]/span/span")).Click();
            Thread.Sleep(5000);

            // Potwierdzenie powyższej operacji
            driver.FindElement(By.XPath("/html/body/div[5]/div[2]/div/div[3]/button[2]")).Click();
            Thread.Sleep(5000);



            /////////////////////////////////////////////////////////////// TEST 4 - DODAWANIE NOWEGO KONTAKTU ///////////////////////////////////////////////////////////////////////



            // Przejście do zakładki "Kontakty"
            driver.FindElement(By.XPath("/html/body/div[2]/nh-app-view/div/nh-top-bar/div/div/div[1]/a[2]")).Click();
            Thread.Sleep(2000);

            // Naciśnięcie przycisku "Nowy kontakt"
            driver.FindElement(By.XPath("/html/body/div[2]/nh-app-view/div/div/div/div[1]/div/div/nh-contacts-header/div/div[1]/button[2]")).Click();
            Thread.Sleep(2000);

            // Wprowadzenie danych nowego kontaktu, wybranie kategorii maila oraz zapisanie zmian
            driver.FindElement(By.Name("firstname")).SendKeys("kontakt-testowy");
            Thread.Sleep(2000);
            driver.FindElement(By.Name("email_value")).SendKeys("kontakt-testowy@wp.pl");
            Thread.Sleep(2000);
            driver.FindElement(By.XPath("/html/body/div[2]/nh-app-view/div/div/div/div[1]/div/div/form/div/div/ol/li[3]/div/div[3]/select")).Click();
            Thread.Sleep(2000);
            driver.FindElement(By.XPath("/html/body/div[2]/nh-app-view/div/div/div/div[1]/div/div/form/div/div/ol/li[3]/div/div[3]/select/option[4]")).Click();
            Thread.Sleep(2000);
            driver.FindElement(By.XPath("/html/body/div[2]/nh-app-view/div/div/div/div[1]/div/div/form/div/div/div/div[2]/button[1]")).Click();
            Thread.Sleep(5000);



            ////////////////////////////////////////////////////////// TEST 5 - PRZENOSZENIE MAILA DO INNEGO FOLDERU /////////////////////////////////////////////////////////////////



            // Przejście do zakładki "Powiadomienia"
            driver.FindElement(By.XPath("/html/body/div[2]/nh-app-view/div/div/div/div[1]/div/div/div/div/div[1]/ul/li[2]/div/span")).Click();
            Thread.Sleep(2000);

            // Zaznaczenie checkbox'a przy ostatnio odebranym mailu
            driver.FindElement(By.XPath("/html/body/div[2]/nh-app-view/div/div/div/div[1]/div/div/div/div/div[2]/div[2]/div[1]/div/div[1]/nh-stream-item/div/div/div/div/div[1]/nh-checkbox/div")).Click();
            Thread.Sleep(2000);

            // Naciśnięcie przycisku "Przenieś do"
            driver.FindElement(By.XPath("/html/body/div[2]/nh-app-view/div/div/div/div[1]/div/div/div/div/nh-top-action-bar/div/div[1]/nh-drop-down/button")).Click();
            Thread.Sleep(2000);

            // Przeniesienie maila do folderu "Odebrane"
            driver.FindElement(By.XPath("/html/body/div[4]/div/div/div[1]/div[1]/div/button")).Click();
            Thread.Sleep(2000);
            driver.FindElement(By.XPath("/html/body/div[2]/nh-app-view/div/div/div/nh-sidebar/div/div/div[3]/div[1]/div[1]/div[2]")).Click();
            Thread.Sleep(5000);



            /////////////////////////////////////////////////////////////// TEST 6 - ZGŁASZANIE MAILA DO SPAMU ///////////////////////////////////////////////////////////////////////



            // Przejście do zakładki "Odebrane"
            driver.FindElement(By.XPath("/html/body/div[2]/nh-app-view/div/div/div/nh-sidebar/div/div/div[3]/div[1]/div[1]/div[2]")).Click();
            Thread.Sleep(2000);

            // Zaznaczenie checkbox'a przy ostatnio odebranym mailu
            driver.FindElement(By.XPath("/html/body/div[2]/nh-app-view/div/div/div/div[1]/div/div/div/div/div[2]/div[2]/div[1]/div/div[1]/nh-stream-item[1]/div/div/div/div/div[1]/nh-checkbox/div")).Click();
            Thread.Sleep(2000);

            // Naciśnięcie przycisku "Zgłoś spam"
            driver.FindElement(By.XPath("/html/body/div[2]/nh-app-view/div/div/div/div[1]/div/div/div/div/nh-top-action-bar/div/div[1]/button[4]")).Click();
            Thread.Sleep(2000);

            // Przejście do zakładki "Spam"
            driver.FindElement(By.XPath("/html/body/div[2]/nh-app-view/div/div/div/nh-sidebar/div/div/div[3]/div[1]/div[5]/div[2]")).Click();
            Thread.Sleep(2000);

            // Naciśnięcie przycisku "Usuń"
            driver.FindElement(By.XPath("/html/body/div[2]/nh-app-view/div/div/div/nh-sidebar/div/div/div[3]/div[1]/div[5]/span/span")).Click();
            Thread.Sleep(5000);

            // Potwierdzenie powyższej operacji
            driver.FindElement(By.XPath("/html/body/div[4]/div[2]/div/div[3]/button[2]")).Click();
            Thread.Sleep(5000);



            /////////////////////////////////////////////////////////// TEST 7 - PRZESYŁANIE ODEBRANEGO MAILA DALEJ //////////////////////////////////////////////////////////////////



            // Przejście do zakładki "Odebrane"
            driver.FindElement(By.XPath("/html/body/div[2]/nh-app-view/div/div/div/nh-sidebar/div/div/div[3]/div[1]/div[1]/div[2]")).Click();
            Thread.Sleep(2000);

            // Przejście do ostatnio odebranego maila
            driver.FindElement(By.XPath("/html/body/div[2]/nh-app-view/div/div/div/div[1]/div/div/div/div/div[2]/div[2]/div[1]/div/div[1]/nh-stream-item[1]/div/div/div/div/div[8]/span")).Click();
            Thread.Sleep(2000);

            // Naciśnięcie przycisku "Prześlij dalej"
            driver.FindElement(By.XPath("/html/body/div[2]/nh-app-view/div/div/div/div[1]/div/div/div[2]/button[2]")).Click();
            Thread.Sleep(5000);

            // Wprowadzenie adresata
            driver.FindElement(By.XPath("/html/body/div[2]/nh-app-view/div/div/div/div[1]/div/div/div/div/div[1]/div[2]/div[2]/div/div/input")).SendKeys("poczta@wp.pl");
            Thread.Sleep(2000);

            // Naciśnięcie przycisku "Wyślij wiadomość"
            driver.FindElement(By.XPath("/html/body/div[2]/nh-app-view/div/div/div/div[1]/div/div/div/div/div[2]/div[4]/div/div[1]/button/div/span")).Click();
            Thread.Sleep(5000);

            // Przejście do zakładki "Wysłane"
            driver.FindElement(By.XPath("//*[@id='folder-2']/div[2]")).Click();
            Thread.Sleep(5000);



            ////////////////////////////////////////////////////////// TEST 8 - WYSŁANIE TESTOWEGO MAILA DO ADRESATA /////////////////////////////////////////////////////////////////



            // Naciśnięcie przycisku "Napisz"
            driver.FindElement(By.XPath("/html/body/div[2]/nh-app-view/div/div/div/div[1]/div/div/div/div/nh-top-action-bar/div/div[1]/button[2]")).Click();
            Thread.Sleep(5000);

            // Wprowadzenie adresata
            driver.FindElement(By.XPath("/html/body/div[2]/nh-app-view/div/div/div/div[1]/div/div/div/div/div[1]/div[2]/div[2]/div/div/input")).SendKeys("poczta@wp.pl");
            Thread.Sleep(2000);

            // Wprowadzenie tematu maila
            driver.FindElement(By.XPath("/html/body/div[2]/nh-app-view/div/div/div/div[1]/div/div/div/div/div[1]/div[3]/div[2]/input")).SendKeys("Testowy mail 1");
            Thread.Sleep(2000);

            // Wprowadzenie treści maila
            driver.FindElement(By.XPath("/html/body/div[2]/nh-app-view/div/div/div/div[1]/div/div/div/div/div[2]/div[2]")).SendKeys("To jest testowy mail numer 1.");
            Thread.Sleep(2000);

            // Naciśnięcie przycisku "Wyślij wiadomość"
            driver.FindElement(By.XPath("/html/body/div[2]/nh-app-view/div/div/div/div[1]/div/div/div/div/div[2]/div[4]/div/div[1]/button/div/span")).Click();
            Thread.Sleep(10000);

            // Przejście do zakładki "Wysłane"
            driver.FindElement(By.XPath("//*[@id='folder-2']/div[2]")).Click();
            Thread.Sleep(5000);



            ////////////////////////////////////////// TEST 9 - ZAPISYWANIE TESTOWEGO MAILA DO WERSJI ROBOCZYCH I WYSYŁANIE GO DO ADRESATA ///////////////////////////////////////////



            // Naciśnięcie przycisku "Napisz"
            driver.FindElement(By.XPath("/html/body/div[2]/nh-app-view/div/div/div/div[1]/div/div/div/div/nh-top-action-bar/div/div[1]/button[2]")).Click();
            Thread.Sleep(5000);

            // Wprowadzenie adresata
            driver.FindElement(By.XPath("/html/body/div[2]/nh-app-view/div/div/div/div[1]/div/div/div/div/div[1]/div[2]/div[2]/div/div/input")).SendKeys("poczta@wp.pl");
            Thread.Sleep(2000);

            // Wprowadzenie tematu maila
            driver.FindElement(By.XPath("/html/body/div[2]/nh-app-view/div/div/div/div[1]/div/div/div/div/div[1]/div[3]/div[2]/input")).SendKeys("Testowy mail 2");
            Thread.Sleep(2000);

            // Wprowadzenie treści maila
            driver.FindElement(By.XPath("/html/body/div[2]/nh-app-view/div/div/div/div[1]/div/div/div/div/div[2]/div[2]")).SendKeys("To jest testowy mail numer 2.");
            Thread.Sleep(5000);

            // Przejście do zakładki "Robocze"
            driver.FindElement(By.XPath("/html/body/div[2]/nh-app-view/div/div/div/nh-sidebar/div/div/div[3]/div[1]/div[3]/div[2]")).Click();
            Thread.Sleep(2000);

            // Przejście do ostatnio zapisanego maila w folderze "Robocze"
            driver.FindElement(By.XPath("/html/body/div[2]/nh-app-view/div/div/div/div[1]/div/div/div/div/div/div[2]/div[1]/div/div[1]/nh-stream-item/div/div/div/div/div[7]/div/div")).Click();
            Thread.Sleep(5000);

            // Naciśnięcie przycisku "Wyślij wiadomość"
            driver.FindElement(By.XPath("/html/body/div[2]/nh-app-view/div/div/div/div[1]/div/div/div/div/div[2]/div[4]/div/div[1]/button/div/span")).Click();
            Thread.Sleep(10000);

            // Przejście do zakładki "Wysłane"
            driver.FindElement(By.XPath("//*[@id='folder-2']/div[2]")).Click();
            Thread.Sleep(5000);



            ////////////////////////////////////////////////////////////////////////// TEST 10 - ZMIANA HASŁA ////////////////////////////////////////////////////////////////////////



            // Naciśnięcie strzałki obok ikony profilu
            driver.FindElement(By.XPath("/html/body/div[2]/nh-app-view/div/nh-top-bar/div/div/div[2]/div[2]/div[1]/button/div/span")).Click();
            Thread.Sleep(2000);

            // Naciśnięcie przycisku "Zmień hasło"
            driver.FindElement(By.XPath("/html/body/div[2]/nh-app-view/div/nh-top-bar/div/div/div[2]/div[2]/div[2]/div/ul/li[3]/a")).Click();
            Thread.Sleep(2000);

            // Wprowadzenie starego hasła
            driver.FindElement(By.Name("oldPassword")).SendKeys("haslo");
            Thread.Sleep(2000);

            // Wprowadzenie nowego hasła
            driver.FindElement(By.Name("password")).SendKeys("nowehaslo");
            Thread.Sleep(2000);

            // Powtórzenie nowego hasła
            driver.FindElement(By.Name("passwordRepeat")).SendKeys("nowehaslo");
            Thread.Sleep(2000);

            // Naciśnięcie przycisku "Zapisz zmiany"
            driver.FindElement(By.XPath("/html/body/div[3]/div[2]/div[2]/div/div[2]/form/div/div[4]/button")).Click();
            Thread.Sleep(5000);

            // Naciśnięcie przycisku "Przejdź do Poczty"
            driver.FindElement(By.XPath("/html/body/div[3]/div[2]/div[1]/div/a[7]/div/div[2]")).Click();
            Thread.Sleep(5000);



            ///////////////////////////////////////////////////////////////////////////// KONIEC /////////////////////////////////////////////////////////////////////////////////////



            // Wylogowanie się z poczty email
            driver.FindElement(By.Id("Logout-Button")).Click();
            Console.ReadKey();

            // Wyjście z programu
            //driver.Close();
            driver.Quit();
        }
    }
}